# MLOps Project for Diabetes Prediction
This repository hosts an MLOps project designed to simulate a real scenario in which a hospital uses a ML model to predict whether a patient has diabetes or not.
In this scenario, the hospital has an historical dataset that will be updated with new data every week, so the ML model needs to be re-trained periodically.
The model is deployed online (many requests are posted per day) via a REST API, and can be used through a GUI.

## Project Description
The project consists of the following main components:

1. **ETL processes**: Airflow is used to first load historical data into a Minio bucket (one-time, manually triggered). Then, another DAG is scheduled weekly to generate synthetic data and load it into the training dataset. Each new data generated has its own timestamp.

2. **Training, retraining and logging model**: through a jupyter notebook, the model is trained and logged in the MLFlow registry. The best model will be put into production. The same notebook can be used (manually) to retrain the model when new data arrives.

3. **Use predictor**: Fast API is used for online deployment. Streamlit provides the app with an easy-to-use GUI.

## Requirements for the client
Ensure the following components are installed beforehand:
- Docker
- Docker-Compose

## Setup and Execution
To run the application, follow these steps:

1. Clone this repository on your local machine, running the following command:

```bash
git clone https://gitlab.com/natanael-ferran-portfolio/mlops-project-for-a-hospital-application.git
```

2. Open the repository in your preferred IDE.

3. Create a virtual environment.

4. Open a terminal and execute:

```bash
make start
```

This command will automatically download and set every needed services. Please wait for about 10 minutes to be finished. Once every service is running without issues, proceed to the next step.

> **Note**: it may not work. If so, please uncomment the line code in the makefile located in `start` and comment the uncommented one. You can also run `docker compose --profile all up`.

4. Open a new terminal and run

```bash
make env
```

This command will automatically install all dependencies needed in your virtual environment.

5. In the same terminal, please run:

```bash
make air
```

This command will open Airflow GUI. Use the following login credentials:

> User = _airflow_

> Password = _airflow_

6. Execute original_data_ETL DAG. Once finished, please proceed to the next step.

7. Unpause continuous_ETL DAG. Once finished, proceed to the next step.

8. You can browse the files created by logging into the Minio GUI. In the terminal, please run:

```bash
make minio
```

Use the following login credentials:

> User = _minio_

> Password = _minio123_

9. In order to train the model, you should run the jupyter notebook located in `/training/train_retrain_model.ipynb`.

10. You can use the app GUI by running in the terminal:

```bash
make app
```

11. You can access the root endpoint of the REST API by running:

```bash
make api
```

And access all the API documentation by running:

```bash
make api_docs
```

12. You can stop every services by running:

```bash
make stop
```

And delete every images by running:

```bash
make delete
```

## Project Structure
```
/airflow/ # Contains airflow data, including the dags.
/dockerfiles/ # Contains all the services data.
/training/ # Contains the jupyter notebook needed to train the model and its auxiliary scripts.
/utils/ # Contains some needed files.
docker-compose.yaml # File which defines and manages the multi-container Docker application.
makefile # Includes simplified commands for easy usage of the application.
README.md # Provides information about the project.
requirements.txt # List of dependencies needed for the virtual environment.
```
## Contributions
Contributions are welcome! If you'd like to improve this project, feel free to submit a push request.

## License
This project is licensed under the [MIT License](https://mit-license.org/).

***

Copyright (c) 2024-present, Natanael Emir Ferrán
