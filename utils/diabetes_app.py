import requests

import streamlit as st


# REST API URL for using diabetes predictor
api_url = "http://localhost:8800/predict/"

# App title
st.title("Diabetes Predictor")

# Form to input patient data
with st.form("patient_data_form"):
    pregnancies = st.number_input(
        "Number of Pregnancies",
        min_value=0, max_value=15, step=1,
        help="Range between 0 and 15."
    )
    glucose = st.number_input(
        "Glucose Level",
        min_value=1, max_value=1000, step=1,
        help="Range between 1 and 1000."
    )
    blood_pressure = st.number_input(
        "Blood Pressure",
        min_value=1, max_value=200, step=1,
        help="Range between 1 and 200."
    )
    skin_thickness = st.number_input(
        "Skin Thickness",
        min_value=1, max_value=50, step=1,
        help="Range between 1 and 50."
    )
    insulin = st.number_input(
        "Insulin",
        min_value=1, max_value=200, step=1,
        help="Range between 1 and 200."
    )
    bmi = st.number_input(
        "BMI",
        min_value=0.1, max_value=70.0, step=0.1,
        help="Range between 0.1 and 70.0."
    )
    diabetes_pedigree_function = st.number_input(
        "Diabetes Pedigree Function",
        min_value=0.001, max_value=50.0, step=0.001,
        help="Range between 0.001 and 50.0."
    )
    age = st.number_input(
        "Age",
        min_value=0, max_value=130, step=1,
        help="Range between 0 and 130."
    )

    # Button to submit form
    submitted = st.form_submit_button("Predict")

    if submitted:
        # Create a dictionary with patient data
        patient_data = {
            "Pregnancies": pregnancies,
            "Glucose": glucose,
            "BloodPressure": blood_pressure,
            "SkinThickness": skin_thickness,
            "Insulin": insulin,
            "BMI": bmi,
            "DiabetesPedigreeFunction": diabetes_pedigree_function,
            "Age": age,
        }

        # Make POST request to the API
        response = requests.post(api_url, json={"features": patient_data})

        # Obtain JSON response from the API
        prediction_result = response.json()

        # Check if the API response is successful
        if response.status_code == 200:
            
            # Show prediction result
            st.write("Prediction Result:")
            if 'int_output' in prediction_result:
                st.write(
                    "Diabetes Detected" if prediction_result['int_output']
                    else "Healthy Patient"
                )
            else:
                st.write("Unexpected error: Please check your input values.")
        else:
            # Display API error message
            st.error(f"Error: {prediction_result.get('detail', 'Invalid input or server error')}")

