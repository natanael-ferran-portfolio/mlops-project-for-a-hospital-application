# Makefile for managing the project

# Detect OS
OS := $(shell uname)

# Command to open URL's depending on the OS:
# - Darwin (macOS): uses 'open'
# - Linux: uses 'xdg-open'
# - Other (e.g. Windows): uses 'start'
ifeq ($(OS),Darwin)
    OPEN_CMD = open
else ifeq ($(OS),Linux)
    OPEN_CMD = xdg-open
else
    OPEN_CMD = start
endif

# Start all services with Docker Compose
# Usage: make start
start:
	docker-compose --profile all up
    # docker compose --profile all up

# Install necessary packages and dependencies
# Usage: make env
env:
	pip install --upgrade pip &&\
		pip install --no-cache-dir -r requirements.txt

# Open Minio web interface
# Usage: make minio
minio:
	$(OPEN_CMD) http://localhost:9001

# Open Apache Airflow web interface
# Usage: make air
air:
	$(OPEN_CMD) http://localhost:8080/login/

# Open MLflow web interface
# Usage: make mlflow
mlflow:
	$(OPEN_CMD) http://localhost:5000

# Open API web interface
# Usage: make api
api:
	$(OPEN_CMD) http://localhost:8800

# Open API documentation
# Usage: make api_docs
api_docs:
	$(OPEN_CMD) http://localhost:8800/docs

# Run the diabetes app with Streamlit
# Usage: make app
app:
	streamlit run utils/diabetes_app.py

# Stop all services and clean up
# Usage: make stop
stop:
	docker-compose --profile all down

# Delete all Docker images and volumes
# Usage: make delete
delete:
	docker-compose down --rmi all --volumes
